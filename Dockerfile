# usando imagen liviana 
FROM node:17-alpine3.14 

# usando el usuario node que viene en la imagen 

#directorio donde copio la app
WORKDIR /home/node/app

#copiando la app
COPY --chown=node:node . .

#instalando la app
RUN npm install

EXPOSE 3000

USER node
 
#ejecutando la app
CMD [ "node", "index.js" ]


